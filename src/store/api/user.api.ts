import axios from "axios";

export const getUser = async (email: string, token: string) => {
  try {
    const result = await axios({
      method: 'get',
      url: 'http://localhost:3000/user/' + email,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
    })
    return result.data;
  } catch (error) {
    console.log('Result IN GET : ', error);
  }
}

export const postUser = async (email: string, firstname: string, lastname: string, token: string) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://localhost:3000/user/signup',
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        lastname: lastname,
        firstname: firstname,
        email: email
      }
    })
    return result.data;
  } catch (error) {
    console.log('Result IN POST : ', error);
  }
}


export const patchUser = async (email: string, firstname: string, lastname: string, token: string) => {
  try {
    const result = await axios({
      method: 'patch',
      url: `http://localhost:3000/user/${email}`,
      headers: {
        'Authorization': 'Bearer ' + token
      },
      data: {
        lastname: lastname,
        firstname: firstname,
        email: email
      }
    })
    console.log(`Result IN PATCH : `, result.data);

    return result.data;
  } catch (error) {
    console.log('Result IN PATCH : ', error);
  }
}

export const getUserByEmail = async (email: string, token: string) => {
  try {
    const result = await axios({
      method: 'get',
      url: 'http://localhost:3000/user/' + email,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
    })
    return result.data;
  } catch (error) {
    console.log('Result IN GET user by email : ', error);
  }
}


