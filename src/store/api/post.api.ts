import axios from "axios";

export const sendPost = async (idUser: number, content: string, token: string) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://localhost:3000/post/' + idUser,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        htmlContent: content
      }
    })
    console.log(`Result IN POST : `, result.data);

    return result.data;
  }
  catch (error) {
    console.log('Result IN POST : ', error);
  }
}

export const getPost = async (token: string, numberByPage: number, numberPage: number) => {
  try {
    const result = await axios({
      method: 'get',
      url: 'http://localhost:3000/post/'+ numberByPage + '/' + numberPage,
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    })
    console.log(`Result IN POST : `, result.data);

    return result.data;
  }
  catch (error) {
    console.log('ERROR IN POST : ', error);
  }
}

export const likePost = async (idPost: number, idUser: number, token: string) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://localhost:3000/post/' + idPost + '/' + idUser + '/like',
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    })
    console.log(`Result IN POST : `, result.data);
    return result.data;
  }
  catch (error) {
    console.log('Result IN POST : ', error);
  }
}

export const unLikePost = async (idPost: number, idUser: number, token: string) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://localhost:3000/post/' + idPost + '/' + idUser + '/unlike',
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    })
    console.log(`Result IN POST : `, result.data);
    return result.data;
  }
  catch (error) {
    console.log('Result IN POST : ', error);
  }
}

export const commentPost = async (idPost: number, idUser: number, text: string, token: string) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://localhost:3000/post/' + idPost + '/' + idUser + '/comment',
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        text: text
      }
    })
    console.log(`Result IN POST : `, result.data);
    return result.data;
  }
  catch (error) {
    console.log('Result IN POST : ', error);
  }
}