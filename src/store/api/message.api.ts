import axios from "axios";

export const getAllMessage = async (idUser: string, token: string) => {
    try{
        const result = await axios({
            method: 'get',
            url: 'http://localhost:3000/conversation/' + idUser,
            headers: {
                'Authorization': 'Bearer ' + token,
            }
        })
        console.log(`Result IN GET : `, result.data);
        return result.data;
    }
    catch (error) {
        console.log('Result IN GET : ', error);
    }
}

export const getConversation = async (idUser: string, idConversation: string, token: string) => {
    try{
        const result = await axios({
            method: 'get',
            url: 'http://localhost:3000/conversation/' + idConversation + '/' + idUser,
            headers: {
                'Authorization': 'Bearer ' + token,
            }
        })
        console.log(`Result IN GET : `, result.data);
        return result.data;
    }
    catch (error) {
        console.log('Result IN GET : ', error);
    }
}

export const newConversation = async (idUser: string, idUser2: string, content: string ,token: string) => {
    try{
        const result = await axios({
            method: 'post',
            url: 'http://localhost:3000/conversation/',
            headers:{
                'Authorization': 'Bearer ' + token,
            },
            data: {
                fromId: idUser,
                toId: idUser2,
                content: content
            }
        })
        console.log(`Result IN POST : `, result.data);
        return result.data;
    }
    catch (error) {
        console.log('Result IN POST : ', error);
    }
}

export const sendMessage = async (idConversation: number, content: string, token: string) => {
    try{
        const result = await axios({
            method: 'post',
            url: 'http://localhost:3000/conversation/message/' + 135,
            headers:{
                'Authorization': 'Bearer ' + token,
            },
            data: {
                conversationId: idConversation,
                content: content
            }
        })
        console.log(`Result IN POST : `, result.data);
        return result.data;
    }
    catch (error) {
        console.log('Result IN POST : ', error);
    }
}