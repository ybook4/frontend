import { IConversation } from "../../types/conversation.type";

export const directMessageList: IConversation[] = [
  {
    id: 2,
    createdAt: new Date('2022-01-02'),
    updatedAt: new Date('2022-01-02'),
    fromId: 1,
    toId: 4
  },
]