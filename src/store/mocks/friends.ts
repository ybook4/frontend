import {IFriendshipRequest, IFriendshipStatus, FriendshipStatusEnum} from '../../types/friendship.type';
import {JohnDoe, MariahCarrey, DwayneJhonson} from './users';

const FriendshipRequest1: IFriendshipRequest = {
  id: 1,
  createdAt: new Date(),
  updatedAt: new Date(),
  from: JohnDoe,
  to: MariahCarrey,
  fromId: 1,
  toId: 2,
  validated: false,
  Notification: []
}

const FriendshipRequest2: IFriendshipRequest = {
  id: 2,
  createdAt: new Date(),
  updatedAt: new Date(),
  from: MariahCarrey,
  to: DwayneJhonson,
  fromId: 2,
  toId: 3,
  validated: false,
  Notification: []
}

const FriendshipRequest3: IFriendshipRequest = {
  id: 3,
  createdAt: new Date(),
  updatedAt: new Date(),
  from: DwayneJhonson,
  to: MariahCarrey,
  fromId: 3,
  toId: 1,
  validated: true,
  Notification: []
}

export {FriendshipRequest1, FriendshipRequest2, FriendshipRequest3}