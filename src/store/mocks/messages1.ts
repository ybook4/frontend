import { IConversationMessage } from "../../types/conversation.type";

const mockedDataMessages: IConversationMessage[] = [
  {
    id: 1,
    createdAt: new Date('2022-01-01'),
    updatedAt: new Date('2022-01-01'),
    conversationId: 2,
    userId: 1,
    content: 'Hello'
  },
  {
    id: 2,
    createdAt: new Date('2022-01-02'),
    updatedAt: new Date('2022-01-02'),
    conversationId: 2,
    userId: 4,
    content: 'Hello'
  },
  {
    id: 3,
    createdAt: new Date('2022-01-03'),
    updatedAt: new Date('2022-01-03'),
    conversationId: 2,
    userId: 1,
    content: 'How are you?'
  },
  {
    id: 4,
    createdAt: new Date('2022-01-04'),
    updatedAt: new Date('2022-01-04'),
    conversationId: 2,
    userId: 4,
    content: 'Fine n U ?'
  },
  {
    id: 5,
    createdAt: new Date('2022-01-05'),
    updatedAt: new Date('2022-01-05'),
    conversationId: 2,
    userId: 1,
    content: 'Fine'
  }
];


export default mockedDataMessages;