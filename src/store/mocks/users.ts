import IUser from "../../types/user.type";

const JohnDoe: IUser = {
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    firstname: "John",
    lastname: "Doe",
    email: "JohnDoe@gmail.com",
    avatarS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
    coverPicS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
}

const MariahCarrey: IUser = {
  id: 1,
  createdAt: new Date(),
  updatedAt: new Date(),
  firstname: "Marriah",
  lastname: "Carrey",
  email: "MarriahCarrey@gmail.com",
  avatarS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
  coverPicS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
}

const DwayneJhonson: IUser = {
  id: 1,
  createdAt: new Date(),
  updatedAt: new Date(),
  firstname: "Dwayne",
  lastname: "Jhonson",
  email: "DwayneJhonson@gmail.com",
  avatarS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
  coverPicS3Key: "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg",
}



export { JohnDoe, MariahCarrey, DwayneJhonson}