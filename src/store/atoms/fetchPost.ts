import { atom } from "jotai";

const needFetchPost = atom<boolean>(false)

export default needFetchPost;