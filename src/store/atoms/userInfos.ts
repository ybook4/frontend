import { atom } from "jotai";
import { CognitoUser } from "amazon-cognito-identity-js";

// user informations atom
const userInfos = atom({
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    avatar: '',
    token: '',
});


const tempPassword = atom('');

const cognitoUserAtom = atom<CognitoUser | null>(null);

const userLoggedIn = atom(false);

export { userInfos, userLoggedIn, cognitoUserAtom, tempPassword };