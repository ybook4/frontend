import React, { useEffect } from 'react';
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom';
import Navbar from '../components/MainViews/Navbar/Navbar';
import routesList from './routesList';
import { useAtom } from 'jotai';
import { userLoggedIn } from '../store/atoms/userInfos';
import Login from '../components/MainViews/Login/Login';

const Router: React.FC = () => {

  const [loggedIn] = useAtom(userLoggedIn);


  return (
    <div id='main' className='h-screen w-screen'>
      <BrowserRouter>
      { loggedIn ?
        <>
          <Routes>
            {routesList.map((route) => (
              <Route path={route.path} element={<route.element />} key={`key_${route.path}`}/>
            ))}
          </Routes>
          <Navbar />
        </>
        : <Login />
      }
        
      </BrowserRouter>
    </div>
  );
};

export default Router;