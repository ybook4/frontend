import React from 'react';
import { Dialog, DialogTitle } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

interface Props {
  isOpened: boolean;
  setIsOpened: React.Dispatch<React.SetStateAction<boolean>>;
  title: string;
  children?: React.ReactNode;
}

const GenericDialog: React.FC<Props> = ({isOpened, setIsOpened, children, title}) => {
  return (
    <Dialog open={isOpened} onClose={() => setIsOpened(false)}>
    <div className="border-b-2 border-gray-100 mr-2 ml-2">
      <div className="w-98 pr-6">
        <DialogTitle>{title}</DialogTitle>
      </div>
      <div className='absolute right-5 top-5 '>
        <CloseIcon onClick={() => setIsOpened(false)} />
      </div>
    </div>
    {children}
  </Dialog>
  );
};

export default GenericDialog;