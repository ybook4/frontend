import { Typography } from '@mui/material';
import { useAtom } from 'jotai';
import React from 'react';
import { getPost } from '../../../store/api/post.api';
import { userInfos } from '../../../store/atoms/userInfos';
import { IPost } from '../../../types/post.type';
import NewPost from '../../SubComponents/NewPost/NewPost';
import Post from '../../SubComponents/Post/Post';
import needFetchPost from '../../../store/atoms/fetchPost';


const Homepage: React.FC = () => {

  const [userInfosAtom] = useAtom(userInfos);
  const [needFetchPostAtom, setNeedFetchPostAtom] = useAtom(needFetchPost);
  const [posts, setPosts] = React.useState<IPost[] | null>(null);
  const [pageNumber, setPageNumber] = React.useState<number>(0);

  const fetchAllPosts = () => {
    // TODO 
    // Ici on peut faire un bouton qui fetch d'autres posts, ou utiliser react inite scroll
    getPost(userInfosAtom.token, 1000, pageNumber).then((res) => {
      setPosts(res.reverse())
    }
    ).catch((err) => {
      alert(err)
    });
  }

  // TODO
  // Ici on fetch régulièrement comme un crado, mais en vrai si on avait les websockets ça serait mieux on utilise un atom quand l'user a envoyé un post
  React.useEffect(() => {
    if (userInfosAtom.token === null) return;
    if (!needFetchPostAtom) fetchAllPosts();
    if (needFetchPostAtom) {  
      fetchAllPosts();
      setNeedFetchPostAtom(false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userInfosAtom.token, needFetchPostAtom, setNeedFetchPostAtom]);

  return (
    <div className='flex flex-col items-center space-y-4 overflow-hidden max-h-screen'>
      <div className='flex justify-center pt-3'>
        <p className='text-3xl font-bold'>Fil d'actualité</p>
      </div>
      <NewPost />
      <div className='overflow-auto flex w-full flex-col items-center ' style={{'marginBottom': '60px'}}>
        <div className='w-full flex flex-col items-center space-y-4'>
          {posts && posts.map((post: IPost) => (
            <Post post={post} key={post.id+'_key'}/>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Homepage;