import React from 'react';
import { CognitoUserPool,CognitoUserAttribute, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { userLoggedIn, userInfos , cognitoUserAtom, tempPassword} from '../../../store/atoms/userInfos';
import { useAtom } from 'jotai';
import RegistrationConfirmer from '../../SubComponents/RegistrationConfirmer/RegistrationConfirmer';
import { useNavigate } from 'react-router-dom';
import { getUser } from '../../../store/api/user.api';

const Login: React.FC = () => {
  const [isRegistered, setIsRegistered] = React.useState(true);
  const [needConfirmation, setNeedConfirmation] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [, setUserInfos] = useAtom(userInfos);
  const [, setLoggedIn] = useAtom(userLoggedIn);
  const [, setTempPassword] = useAtom(tempPassword);
  const [, setCognitoUser] = useAtom(cognitoUserAtom);
  const nav = useNavigate();
  const inputClassName = "mt-4 bg-gray-50 border-2 border-bg-[#3F96C4] text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
  const poolData = {
    UserPoolId: 'eu-west-3_Iekd8jDeb',
    ClientId: '7llslfb09dtag2h7117og3ku72',
  };
  const userPool = new CognitoUserPool(poolData);

  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.persist();
    if (isRegistered) {
      login(event);
    } else {
      register(event);
      setNeedConfirmation(true);
    }
  }

  const login = (event: React.FormEvent<HTMLFormElement>) => {
    nav('/publication');
    const data = new FormData(event.currentTarget);
    const authData = {
      Username: data.get('email') as string,
      Password: data.get('password') as string,
    }
    const authenticationDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: data.get('email') as string,
      Pool: userPool,
    }
    const cognitoUser = new CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        const JWTToken = result.getIdToken().getJwtToken();
        cognitoUser.getUserAttributes(async function(err, result) {
          if (err) {
            alert(err.message || JSON.stringify(err));
            return;
          } else if (result) {
            const backendUserInfos = await getUser(result[4].Value, JWTToken)
            const user = {
              id: backendUserInfos.id,
              email: result[4].Value,
              firstName: backendUserInfos.firstname,
              lastName: backendUserInfos.lastname,
              avatar: '',
              token : JWTToken,
            };
            setUserInfos(user);
            setCognitoUser(cognitoUser)
            
          }
        });
        setLoggedIn(true);
      },
      onFailure: function(err) {
        alert(err.message || JSON.stringify(err));
      },
    });
  }

  const register = (event: React.FormEvent<HTMLFormElement>) => {
    nav("/publication")
    const data = new FormData(event.currentTarget);
    const userAttributes = [];
    const email = {
      Name: 'email',
      Value: data.get('email') as string,
    };
    const password = {
      Name: 'password',
      Value: data.get('password') as string,
    };
    const firstName = {
      Name: 'given_name',
      Value: data.get('firstName') as string,
    };
    const lastName = {
      Name: 'name',
      Value: data.get('lastName') as string,
    };
    const attributeEmail = new CognitoUserAttribute(email);
    const attributeFirstName = new CognitoUserAttribute(firstName);
    const attributeLastName = new CognitoUserAttribute(lastName);
    userAttributes.push(attributeFirstName);
    userAttributes.push(attributeLastName);
    userAttributes.push(attributeEmail);
    setEmail(email.Value);
    userPool.signUp(email.Value, password.Value, userAttributes, userAttributes, function(
      err,
      result
    ) {
      if (err) {
        alert(err.message || JSON.stringify(err));
        return;
      }
      const cognitoUser = result?.user;
      const user = {
        id: 0,
        email: cognitoUser?.getUsername() as string,
        firstName: attributeFirstName.Value,
        lastName: attributeLastName.Value,
        avatar: '',
        token : '',
      };
      setTempPassword(password.Value);
      setUserInfos(user);
    });
  }

  return (
    needConfirmation ? (
      <RegistrationConfirmer 
        userPool={userPool}
        email = {email}
        registeredSetter={setIsRegistered}
        setNeedConfirmation={setNeedConfirmation}
      />
    ) : (
    <div className='flex flex-col items-center'>
    <form className="flex justify-center mt-14" onSubmit={submit}>
      <img src="https://media.tenor.com/4BkYeeOwEUgAAAAC/waving-wave.gif" alt="wave" className="absolute top-0 left-0 w-20 h-20"/>
      <img src="https://cdn.pixabay.com/photo/2020/12/27/20/24/smile-5865208_1280.png" alt="smiley happy" className='w-16' />
      <img src="https://media.tenor.com/4BkYeeOwEUgAAAAC/waving-wave.gif" alt="wave" className="absolute top-0 right-0 w-20 h-20 transform -scale-x-100"/>
      <div className="flex flex-col absolute top-48">
        <input 
          name="email" 
          type="email" 
          placeholder="Email"
          className={inputClassName}
        />
        <input 
          name="password" 
          type="password" 
          placeholder="Mot de passe" 
          className={inputClassName}
        />
        {!isRegistered && 
          <>
            <input 
              name="password2" 
              type="password"   
              placeholder="Repetez le mot de passe" 
              className={inputClassName}
            />
            <input 
            name="firstName" 
            type="text" 
            placeholder="Prénom" 
            className={inputClassName}
            />
            <input 
              name="lastName" 
              type="text" 
              placeholder="Nom" 
              className={inputClassName}
            />
          </> 
        }
        <div className='flex flex-col items-center p-5 space-y-5'>
          <button type="submit" className="w-32 p-2 bg-[#3F96C4] text-white rounded-md">
            {!isRegistered ? "S'enregistrer" : "Connexion"}
          </button>
        </div>
      </div>
    </form>
    <button onClick={() => setIsRegistered(!isRegistered)} className="bg-[#3F96C4] p-2 rounded-md text-white mt-4">
      {isRegistered ? 'Pas encore de compte ?' : 'Déjà un compte ?'}
    </button>
  </div>
)
)};

export default Login;