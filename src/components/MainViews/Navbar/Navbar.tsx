import React, { useCallback, useEffect } from 'react';
import routesList from '../../../router/routesList';
import { useNavigate, useLocation } from 'react-router-dom';

const Navbar: React.FC = () => {

  const location = useLocation();
  const nav = useNavigate();

  //  Only re calculated if location has changed  
  const isActive = useCallback((path: string) => location.pathname.startsWith(path) && location.pathname !== "/", [location])

  //useEffect to redirect to /home if the url is /
  useEffect(() => {location.pathname === "/" && nav("/home")}, [location, nav])

  return (
	    <section className="absolute bottom-0 z-10 w-full h-15 bg-gray-400 p-3">
        <div className="flex justify-around">
            {routesList.map((route) => (
              <route.icon 
                fontSize={30} 
                color={isActive(route.path) ? '#3F96C4' : 'black' }
                onClick={() => nav(route.path)} 
                key={`button_redirect_${route.element}`}
              />
            ))}
        </div>
      </section>
    );
};

export default Navbar;