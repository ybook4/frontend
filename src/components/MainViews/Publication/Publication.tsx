import React from 'react';
import SendIcon from '@mui/icons-material/Send';
import {IPost} from '../../../types/post.type';
import Post from '../../SubComponents/Post/Post';
import { commentPost, getPost, likePost, sendPost, unLikePost } from '../../../store/api/post.api';
import {userInfos} from '../../../store/atoms/userInfos';
import { useAtom } from 'jotai';

const Publication: React.FC = () => {
  
  const [content, setContent] = React.useState<string>('');
  const [userInfosAtom] = useAtom(userInfos);


  const tempPost: IPost = {
    id: 1,
    htmlContent: content,
    updatedAt: new Date(),
    createdAt: new Date(),
    userId: 1,
  };
  
  const createPost = () => {
    sendPost(userInfosAtom.id,content, userInfosAtom.token);
    setContent('');
    alert('Votre publication a bien été envoyée !')
  }
  return (
    <div>
      <div className='flex justify-center h-full pb-10 pt-3'>
        <p className='text-3xl font-bold'>Nouvelle publication</p>
      </div>
      <div className="flex flex-col mt-5">
          <textarea
            className="min-w-92 w-92 mr-5 ml-5 h-36 border-2 border-gray-100 rounded-md"
            placeholder="Qu'est ce que ça raconte ?"
            onChange={(e) => setContent(e.target.value)}
            maxLength={141}
            style={{'resize': 'none'}}
            value={content}
          />
          <p className='text-slate-400 text-sm absolute right-10 mt-28'>{content.length} / 141</p>
          <button>
            <div onClick={() => createPost()} className='flex absolute right-10 mt-3 text-[#3F96C4] border-2 border-[#3F96C4] p-2 rounded-md hover:bg-[#3F96C4] hover:text-white'>
              <p>Envoyer</p> <SendIcon className='ml-2' />
            </div>
          </button>
        </div>
        <div className="flex flex-col justify-center items-center p-4 pt-16">
          <p className="pb-8 text-black font-bold">Prévisualisation</p>
          <Post post={tempPost} isPublication/>
        </div>
    </div>
  );
};

export default Publication;