import React from 'react';
import FriendLabel from '../../SubComponents/FriendLabel/FriendLabel';
import { FriendshipRequest1, FriendshipRequest2, FriendshipRequest3 } from '../../../store/mocks/friends';
import { IFriendshipRequest } from '../../../types/friendship.type';
const Friends: React.FC = () => {

  const [friendshipRequests, setFriendshipRequests] = React.useState<IFriendshipRequest[]>([]);

  const validateFriendshipRequest = (friendship: IFriendshipRequest) => {
    const newFriendshipRequests = friendshipRequests.map((friendshipRequest: IFriendshipRequest) => {
      if (friendshipRequest.id === friendship.id) {
        return {
          ...friendshipRequest,
          validated: true
        }
      }
      return friendshipRequest;
    });
    setFriendshipRequests(newFriendshipRequests);
  }

  const denyFriendshipRequest = (friendship: IFriendshipRequest) => {
    const newFriendshipRequests = friendshipRequests.filter((friendshipRequest: IFriendshipRequest) => {
      return friendshipRequest.id !== friendship.id;
    });
    setFriendshipRequests(newFriendshipRequests);
  }

  React.useEffect(() => {
    setFriendshipRequests([FriendshipRequest1, FriendshipRequest2, FriendshipRequest3]);
  }, []);
  
  return (  

    <div className="bg-transparent w-screen h-screen flex items-center flex-col space-y-10">
      <div className='flex justify-center pb-10 pt-3'>
        <p className='text-3xl font-bold'>Amis</p>
      </div>
      <div className="border-2 border-black w-96 rounded-md ml-1 mt-1 flex flex-col items-center pb-10">
        <p className="text-center">Friendship Request</p>
        {friendshipRequests.map((friendship: IFriendshipRequest) => (
              friendship.validated === false && 
                <FriendLabel 
                  isPending 
                  friendship={friendship} 
                  key={friendship.id+'_key'} 
                  validateFriendshipRequest={validateFriendshipRequest} 
                  denyFriendshipRequest={denyFriendshipRequest}  
                />
        ))}
      </div>

      <div className="bg-gray-200 h-96 border-2 border-gray-50 rounded-md ml-1 flex flex-col items-center w-full mr-1">
        <p className="text-center">Friends</p>
          {friendshipRequests.map((friendship: any) => (
            friendship.validated === true && <FriendLabel isPending={false} friendship={friendship} key={friendship.id+'_key'}/>
          ))}
      </div>
         
    </div>
  );
};

export default Friends;