import { useAtom } from 'jotai';
import React from 'react';
import { userLoggedIn, userInfos, cognitoUserAtom } from '../../../store/atoms/userInfos';
import avatarImg from '../../../assets/avatar.jpg';
import { BsFillPencilFill } from 'react-icons/bs';
import { AiFillSave } from 'react-icons/ai';
import { patchUser } from '../../../store/api/user.api';
import {BsTrashFill} from 'react-icons/bs';

const Profile: React.FC = () => {

  const [, setUserLoggedIn] = useAtom(userLoggedIn);
  const [userInfosAtom, setUserInfos] = useAtom(userInfos);
  const [cognitoUser, setCognitoUser] = useAtom(cognitoUserAtom);


  const [isEditingFirstName, setIsEditingFirstName] = React.useState(false);
  const [prenom, setPrenom] = React.useState(userInfosAtom.firstName);

  const [isEditingLastName, setIsEditingLastName] = React.useState(false);
  const [nom, setNom] = React.useState(userInfosAtom.lastName);

  const [email,] = React.useState(userInfosAtom.email);

  const logout = () => {
    cognitoUser?.signOut();
    setUserLoggedIn(false);
    setUserInfos({
      id: 0,
      firstName: '',
      lastName: '',
      email: '',
      avatar: '',
      token: '',
    });
    setCognitoUser(null);
  };

  const updateUser = () => {
    patchUser(email, prenom, nom, userInfosAtom.token)
    setIsEditingFirstName(false);
    setIsEditingLastName(false);
    setUserInfos({
      id: userInfosAtom.id,
      firstName: prenom,
      lastName: nom,
      email: userInfosAtom.email,
      avatar: userInfosAtom.avatar,
      token: userInfosAtom.token,
    })
  }

  return (
    <div>
      <div className='flex justify-center'>
        <p className='text-3xl font-bold'>Profil</p>
      </div>
      <div className='flex justify-end absolute top-4 right-4'>
        <button onClick={logout} className='w-30 border-2 border-[#ff1717]	p-4 py-1 rounded-md hover:bg-[#ffbdbd]'>
          Logout
        </button>
      </div>

      <div id='profile' className='flex flex-col flex-center items-center mt-2 space-y-4' >
        <img
          alt="User avatar"
          src={avatarImg}
          className="h-24 w-24 rounded-full shadow-md shadow-[#3F96C4] object-cover mx-auto mt-2 drop-shadow-none"
        />

        <div className='flex flex-row flex-center mt-2'>
          <p>Prenom : </p>
          {isEditingFirstName
            ? <div className='flex'>
              <input type='text' value={prenom} onChange={(e: any) => setPrenom(e.target.value)} className='border-2 border-[#000000] w-60 ' />
              <button onClick={() => updateUser()}><AiFillSave className="hover:text-orange-900 mx-3.5" /></button>
            </div>
            : <p>{prenom}</p>
          }
          {!isEditingFirstName && <button onClick={() => setIsEditingFirstName(true)}><BsFillPencilFill className="hover:text-orange-900 mx-3.5" /></button>}
        </div>

        <div className='flex flex-row flex-center'>
          <p>Nom : </p>
          {isEditingLastName
            ? <div className='flex'>
              <input type='text' value={nom} onChange={(e: any) => setNom(e.target.value)} className='border-2 border-[#000000] w-60 ' />
              <button onClick={() => updateUser()}><AiFillSave className="hover:text-orange-900 mx-3.5" /></button>
            </div>
            : <p>{nom}</p>
          }
          {!isEditingLastName && <button onClick={() => setIsEditingLastName(true)}><BsFillPencilFill className="hover:text-orange-900 mx-3.5" /></button>}
        </div>
        
        <div className='flex flex-row flex-center'>
          <p>Email : {email} </p>
        </div>

        <div className='border-2 rounded-md w-11/12 flex flex-col items-center h-40 space-y-5'>
          <p className='font-bold'>Gestion de compte</p>
          <div className='flex flex-col items-center justify-center h-23 space-y-4 '>
            <p className='text-center'>Vous pouvez supprimer votre compte en cliquant sur le bouton ci-dessous. </p>
            <button className='border-2 border-[#ff1717]	p-4 py-1 rounded-md hover:bg-[#ffbdbd]'><BsTrashFill/></button>
          </div>
        </div> 
      </div>

      <div className='absolute w-screen bottom-12'>
        <p className='text-slate-400 text-center'>En cas de problème contactez :  </p>
        <p className='text-slate-400 text-center'>ybook-les-peintres@ynov.com</p>
        <img src="https://cdn.automobile-propre.com/uploads/2019/10/nouvelle-peugeot-208-spot-tv-730x421.jpg" alt="pub" />
      </div>

    </div>
  );
};

export default Profile;