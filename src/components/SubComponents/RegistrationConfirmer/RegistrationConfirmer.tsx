import React from 'react';
import {
  AuthenticationDetails,
  CognitoUser,
	CognitoUserPool,
} from 'amazon-cognito-identity-js';
import { userLoggedIn, userInfos, cognitoUserAtom, tempPassword } from '../../../store/atoms/userInfos';
import { useAtom } from 'jotai';
import { postUser } from '../../../store/api/user.api';

interface Props {
  userPool : CognitoUserPool,
  email: string,
  registeredSetter: React.Dispatch<React.SetStateAction<boolean>>,
  setNeedConfirmation: React.Dispatch<React.SetStateAction<boolean>>,
}

const RegistrationConfirmer: React.FC<Props> = ({userPool, email, registeredSetter, setNeedConfirmation}) => {
  const userData = {
    Username: email,
    Pool: userPool,
  };
  const cognitoUser = new CognitoUser(userData);
  const [confirmationCode, setConfirmationCode] = React.useState('');
  const [data, setUserInfos] = useAtom(userInfos);
  const [, setCognitoUser] = useAtom(cognitoUserAtom);
  const [, setLoggedIn] = useAtom(userLoggedIn);
  const [passwordAtom, ] = useAtom(tempPassword);

  const confirm = () => {
    cognitoUser.confirmRegistration(confirmationCode, true, (err, result) => {
      if (err) {
        return;
      }
      registeredSetter(true);
      setNeedConfirmation(false);
    });
    firstLogin();
  }

  const firstLogin = () => {
    const authData = {
      Username: data.email,
      Password: passwordAtom,
    }
    var authenticationDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: data.email,
      Pool: userPool,
    }
    let tempUserInformation = {
      email: '',
      firstname: '',
      lastname: '',
      token: '',
    }
    const cognitoUser = new CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        const JWTToken = result.getIdToken().getJwtToken();
        cognitoUser.getUserAttributes(async function(err, result) {
          if (err) {
            alert(err.message || JSON.stringify(err));
            return;
          } else if (result) {
            const user = {
              id: 0,
              email: result[4].Value,
              firstName: result[2].Value,
              lastName: result[3].Value,
              avatar: '',
              token : JWTToken,
            };
            tempUserInformation = {
              email: result[4].Value,
              firstname: result[2].Value,
              lastname: result[3].Value,
              token: JWTToken,
            }
            setUserInfos(user);
            setCognitoUser(cognitoUser)
            const userReturned = await postUser(tempUserInformation.email, tempUserInformation.firstname, tempUserInformation.lastname, tempUserInformation.token);
            setUserInfos({
              id: userReturned.id,
              email: userReturned.email,
              firstName: userReturned.firstName,
              lastName: userReturned.lastName,
              avatar: userReturned.avatar,
              token: userReturned.token,
            })
            setLoggedIn(true);
          }
        });
        

      },
      onFailure: function(err) {
        alert(err.message || JSON.stringify(err));
      },
    });
  } 

  return (
    <>
      <p>You just recieved an email with a confirmation code at </p>
      <p>{email}</p>
      <input 
        type="text" 
        placeholder="Confirmation Code" 
        value={confirmationCode} 
        onChange={(e) => {setConfirmationCode(e.target.value)}}
      />
      <button onClick={confirm}>Confirm</button>
    </>
    
  );
};

export default RegistrationConfirmer;