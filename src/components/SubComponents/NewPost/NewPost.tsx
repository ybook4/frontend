import React from 'react';
import GenericDialog from '../../GenericComponents/GenericDialog/GenericDialog';
import SendIcon from '@mui/icons-material/Send';
import { useAtom } from 'jotai';
import { userInfos } from '../../../store/atoms/userInfos';
import { sendPost } from '../../../store/api/post.api';
import needFetchPost from '../../../store/atoms/fetchPost';

const NewPost: React.FC = () => {

  const [open, setOpen] = React.useState<boolean>(false);
  const [content, setContent] = React.useState<string>('');
  const [, setNeedFetchPostAtom ] = useAtom(needFetchPost)
  const [userInfosAtom, ] = useAtom(userInfos)

  const createPost = () => {
    sendPost(userInfosAtom.id,content, userInfosAtom.token);
    setContent('');
    alert('Votre publication a bien été envoyée !')
    setNeedFetchPostAtom(true);
    setOpen(false);
  }

  return (
    <div>
      <button onClick={() => setOpen(true)}
        className="bg-gray-200 w-80 h-12 border-2 border-gray-50 rounded-md m-t-5"
      >
        Publication Rapide
      </button>

      <GenericDialog 
        title='Votre publication'
        isOpened={open} 
        setIsOpened={setOpen}
      >
        <div className="flex flex-col mt-5">
          <textarea
            style={{resize: 'none'}}
            className="min-w-92 w-92 mb-12 mr-5 ml-5 h-32 border-2 border-gray-100 rounded-md"
            placeholder="Qu'est ce qu'on raconte ?"
            onChange={(e) => setContent(e.target.value)}
            value={content}
          />
          <button className="absolute right-0 bottom-0 mr-2 mb-2" onClick={createPost}>
            <SendIcon />
          </button>
        </div>
      </GenericDialog>
    </div>
  );
};

export default NewPost;