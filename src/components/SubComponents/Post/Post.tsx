import React from 'react';
import { IPost } from '../../../types/post.type';
import avatarImg from '../../../assets/avatar.jpg';
import moment from 'moment';
import { FcLike } from 'react-icons/fc';
import { AiFillHeart, AiOutlineComment, AiOutlineHeart } from 'react-icons/ai';
import GenericDialog from '../../GenericComponents/GenericDialog/GenericDialog';
import Comments from './Comments/Comments';
import { userInfos } from '../../../store/atoms/userInfos';
import { useAtom } from 'jotai';

interface Props {
  post : IPost;
  isPublication?: boolean
}

const Post: React.FC<Props> = ({post, isPublication}) => {
  
  const [userInfo,] = useAtom(userInfos);
  const [userName, setUserName] = React.useState<string>('Barack OBAMA');
  const [postLiked, setPostLiked] = React.useState<boolean>(false);
  const [openComments, setOpenComments] = React.useState<boolean>(false);
  const [counter, setCounter] = React.useState(0);
  const data = post.htmlContent 
  const userNamePost = 'A random user'

  React.useEffect(() => {
    if (userName.length > 15) {
      setUserName(userName.slice(0, 15) + '...');
    }
  }, [userName]);


  // TODO 
  // Manque une route sur le back pour récupérer le nom de l'user par son id 
  const getPostUser = () => {}

  const likePost = () => {
    setPostLiked(!postLiked);
    setCounter(postLiked ? counter - 1 : counter + 1)
  };

  const commentPost = () => {
    setOpenComments(true);
  };

  return (
    <div className="border-2 border-ligthGray flex flex-col justify-center w-5/6 rounded-md drop-shadow-lg relative h-48">
      <div className=" absolute top-0 bg-transparent shadow-none flex align-center ">
        <img 
          alt="User avatar" 
          src={avatarImg} 
          className="h-12 w-12 rounded-full object-cover mx-auto mt-2 drop-shadow-none ml-2	"
        />
        <p className="font-bold ml-2 mt-2 overflow-auto">
          {userNamePost}
        </p>        
      </div>
      <div className="absolute right-0 top-0 mr-2 mt-2">
        <p className="text-slate-400">
          {moment(post.updatedAt).format('DD/MM/YYYY')}        
        </p>
        <p className="text-slate-400 absolute right-0">
          {moment(post.updatedAt).format('hh:mm')} 
        </p>
      </div>

      <div className='relative mt-8 ml-16 h-full mr-10 overflow-scroll' dangerouslySetInnerHTML={{__html: data}} />

      {!isPublication && (
        <>
          <div className="absolute left-2 bottom-2 flex flex-row items-center">
            <p className="text-slate-400 flex items-center">
            {counter} <FcLike />
            </p>
          </div>
          <div className="absolute right-2 bottom-2 flex">
            <AiOutlineComment size={30} onClick={() => commentPost()}/>
            {postLiked ? (
              <AiFillHeart size={30} color={'red'} onClick={() => likePost()}/>
            ):
            (
              <AiOutlineHeart size={30} onClick={() => likePost()}/>
            )}
          </div>
          <GenericDialog isOpened={openComments} setIsOpened={setOpenComments} title='Commentaires'>
              <Comments post={post} />
          </GenericDialog>
        </>
      )
      }
    </div>
  );
};

export default Post;