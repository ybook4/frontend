import React from 'react';
import { IPost } from '../../../../types/post.type';
import Post from '../Post';

interface Props {
  post: IPost
}


const Comments: React.FC<Props> = ({post}) => {

  return (
    <div className='w-screen flex flex-col'>
      <Post post={post} />
      <h1>Comments</h1>
      <p>Comments</p>
    </div>
  );
};

export default Comments;