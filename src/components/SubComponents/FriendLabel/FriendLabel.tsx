import React from 'react';
import { AiFillPlusCircle } from 'react-icons/ai';
import { IFriendshipRequest } from '../../../types/friendship.type';
import { MdCancel } from 'react-icons/md';

interface Props {
  friendship : IFriendshipRequest;
  isPending: boolean
  validateFriendshipRequest?: (friendship: IFriendshipRequest) => void;
  denyFriendshipRequest?: (friendship: IFriendshipRequest) => void;
}

const FriendLabel: React.FC<Props> = ({friendship, isPending, validateFriendshipRequest, denyFriendshipRequest}) => {

  return (
    <div 
    className='flex flex-row items-center justify-center mt-2 mb-2 w-4/5
    rounded-lg h-16 border-[#3F96C4] border-2 shadow-md shadow-[#3F96C4]
    '  
    >
      <p>{friendship.from.firstname}</p>
      {isPending && validateFriendshipRequest && denyFriendshipRequest &&(
        <div className='flex flex-row items-center justify-center ml-2'>
            <AiFillPlusCircle color='#3F96C4' size={40} onClick={() => validateFriendshipRequest(friendship)}/>
            <MdCancel color='red' size={40} onClick={() => denyFriendshipRequest(friendship)}/>
        </div>)
        }
    </div>
  );
};

export default FriendLabel;