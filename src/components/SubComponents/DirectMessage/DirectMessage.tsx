import React, { useEffect, useState } from 'react';
import { IConversation } from '../../../types/conversation.type';
import {AiOutlinePlus} from 'react-icons/ai';
import GenericDialog from '../../GenericComponents/GenericDialog/GenericDialog';
import SendIcon from '@mui/icons-material/Send';
import { IoIosArrowBack } from 'react-icons/io';
import { useAtom } from 'jotai';
import { userInfos } from '../../../store/atoms/userInfos';
import MessageSkeleton from './MessageSkeleton/MessageSkeleton';
import { getAllMessage } from '../../../store/api/message.api';

const DirectMessage = () => {
  const [directMessageOpened, SetDirectMessageOpened] = useState<boolean>(false);
  const [directMessageSelected, SetDirectMessageSelected] = useState<IConversation | null >(null);
  const [openNewMessageDialog, SetOpenNewMessageDialog] = useState<boolean>(false);
  const [dmList, setDmList] = useState<IConversation[] | null>(null);
  const [userInfosAtom, ] = useAtom(userInfos)


  const getAllConversation = () => {
    if (userInfosAtom.id === 0) return
      getAllMessage(userInfosAtom.id.toString(), userInfosAtom.token).then((res) => {
      setDmList(res)
    }
      ).catch((err) => { 
        alert(err)
      });
  }

  const sendMessage = () => {
    SetOpenNewMessageDialog(false);
  }

  const getBackToDirectMessageList = () => {
    SetDirectMessageSelected(null);
    SetDirectMessageOpened(false);
  }

  const enterInDirectMessage = (conversation: IConversation) => {
    SetDirectMessageSelected(conversation);
    SetDirectMessageOpened(true);
  }

  useEffect(() => {
    getAllConversation()
  }, [])

  return (
    <div className='flex flex-col'>

      <div id='discussionHeader' className='top-0 h-16 w-screen'>
        {directMessageOpened ?
        <div>
          <div onClick={getBackToDirectMessageList} className='flex flex-row items-center justify-center mt-2'> 
            <IoIosArrowBack />Retours a la liste des DM
          </div>
        </div>
        : 
        <div className='flex justify-center h-full pb-10 pt-3'>
          <p className='text-3xl font-bold'>Messages Privés</p>
        </div>
        }
      </div>

      {directMessageOpened && directMessageSelected ?
        <div className='flex flex-col'>

          <MessageSkeleton directMessageSelected={directMessageSelected} />
        </div>
        :
        <div className='flex flex-col w-full items-center pl-4 pr-4'>
           {dmList?.map((conversation) => (
            <div 
            key={conversation.id} 
            onClick={() => enterInDirectMessage(conversation)}
            className='flex flex-row items-center justify-center mt-2 cursor-pointer mb-2 w-full 
            rounded-lg h-16 border-[#3F96C4] border-2 shadow-md shadow-[#3F96C4]
            hover:bg-[#3F96C4] hover:text-white transition-all duration-300
            '  
            >
              Message privé avec {userInfosAtom.id === conversation.fromId ? conversation.toId : conversation.fromId}
            </div>
           ))}
        </div>
      }

      {!directMessageOpened &&
        <div className='absolute right-4 bottom-20 w-18 border-2 rounded-full p-4'>
          <AiOutlinePlus onClick={() => SetOpenNewMessageDialog(true)}/>
            <GenericDialog isOpened={openNewMessageDialog} setIsOpened={SetOpenNewMessageDialog} title='Nouveau Message'>
            <div className='flex flex-col h-96 items-center'>
              <input placeholder='A Qui ?' type='text' className='border-2 mb-6 w-11/12 mt-2'/>
              <textarea placeholder='Message' className='h-44 w-11/12 border-2' style={{resize: 'none'}}/>
              <button>
                <div onClick={sendMessage} className='flex absolute right-2.5 mt-3 text-[#3F96C4] border-2 border-[#3F96C4] p-2 rounded-md hover:bg-[#3F96C4] hover:text-white'>
                  <p>Envoyer</p> <SendIcon className='ml-2' />
                </div>
              </button>
            </div>
          </GenericDialog>
        </div>
      }
  </div>
  );
};

export default DirectMessage;
