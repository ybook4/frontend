import React, { useEffect, useState } from 'react';
import { IConversation, IConversationMessage } from '../../../../types/conversation.type';
import mockedDataMessages from '../../../../store/mocks/messages1';
import SimpleMessage from '../SimpleMessage/SimpleMessage';
import {GrSend} from 'react-icons/gr';
import { sendMessage } from '../../../../store/api/message.api';
import { userInfos } from '../../../../store/atoms/userInfos';
import { useAtom } from 'jotai';
import { getConversation } from '../../../../store/api/message.api';
interface Props {
  directMessageSelected: IConversation;
}

const MessageSkeleton : React.FC<Props> = ({directMessageSelected}) => {
  const [userInfosAtom, ] = useAtom(userInfos)
  const [messages, setMessages] = useState<IConversationMessage[]>(mockedDataMessages);
  const convRef = React.useRef<HTMLDivElement>(null);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    const input = e.target.elements.message;
    // Ca ne marche pas, on reçoit un NaN du backend.
    sendMessage(directMessageSelected.id, input.value, userInfosAtom.token)
    input.value = '';
  };

  useEffect(() => {
    convRef.current?.scrollTo(0, convRef.current?.scrollHeight);
  }, [messages]);

  const recieveMessage = () => {
    getConversation(userInfosAtom.id.toString(), directMessageSelected.id.toString(), userInfosAtom.token).then((res) => {
      setMessages(res.messages)
    }).catch((err) => {
      alert(err)
    }
    )
  }

  useEffect(() => {
    recieveMessage()
  }, [])

  return (
    <div className=" h-full pl-3 flex">
        <div className="overflow-y-auto w-full mb-28 pr-3" ref={convRef}>
          {messages.map((message, index) => (
            directMessageSelected.id === message.conversationId &&
              <SimpleMessage message={message} key={message.id+'_key_message_skeleton'}/>
          ))}
        </div>
        <form onSubmit={handleSubmit} className="bg-white flex p-2 absolute bottom-0 mb-14 w-full -ml-4">
          <input
            className="p-2 rounded-md w-full mr-3 border-2 border-gray-300"
            type="text"
            placeholder="Entrez votre message"
            name="message"
            autoComplete='off'
          />
          <button className="bg-[#3F96C4] text-white p-4 rounded-md">
            <GrSend />
          </button>
        </form>
    </div>
  );
};

export default MessageSkeleton;
