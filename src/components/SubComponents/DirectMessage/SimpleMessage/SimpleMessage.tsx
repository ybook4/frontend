import { useAtom } from 'jotai';
import React from 'react';
import { userInfos } from '../../../../store/atoms/userInfos';
import { IConversationMessage } from '../../../../types/conversation.type';

interface Props {
  message: IConversationMessage;
}

const SimpleMessage: React.FC<Props> = ({message}) => {

  const [userInfo,] = useAtom(userInfos);
  
  return (
    userInfo.id === message.userId ? (
      <div className="bg-[#3F96C4] p-2 rounded-lg mb-2 ml-10">
        <p className="text-white">{message.content}</p>
      </div>
    ) : (
      <div className="bg-gray-700 p-2 rounded-lg mb-2 mr-10">
        <p className="text-white">{message.content}</p>
      </div>
    )
  );
};

export default SimpleMessage;